ALTER USER postgres WITH ENCRYPTED PASSWORD 'M1nopex*()';
CREATE USER grafana WITH ENCRYPTED PASSWORD 'Gr@F@N@!!';
CREATE DATABASE grafana;
GRANT ALL PRIVILEGES ON DATABASE grafana TO grafana;

-----------------

CREATE TABLE public.raw_data
(
    rt_timestamp timestamp with time zone NOT NULL,
    rt_EntityID uuid,
    rt_value double precision,
    rt_quality integer,
    primary key (rt_timestamp,rt_EntityID)
)
TABLESPACE pg_default;

ALTER TABLE public.raw_data
    OWNER to postgres; 

-----------------

CREATE INDEX kd_rawdata_ts ON public.raw_data USING btree (rt_timestamp);
CREATE INDEX kd_rawdata_uuid ON public.raw_data USING btree (rt_EntityID);

-------------------

CREATE USER minopex_eng WITH PASSWORD 'M1nopexEng!';

GRANT all ON TABLE public.raw_data TO minopex_eng;

CREATE EXTENSION IF NOT EXISTS timescaledb;

-- create hypertables needed
---
SELECT create_hypertable('raw_data', 'rt_timestamp', 
chunk_time_interval => interval '1 week');

--- compression strategy
ALTER TABLE raw_data SET (
  timescaledb.compress,
  timescaledb.compress_segmentby = 'rt_EntityID'
);


SELECT add_compression_policy('raw_data', INTERVAL '1 month');

---------------

CREATE EXTENSION pg_stat_statements;
