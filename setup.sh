##Ubuntu:
	#Update OS and install required packages
	apt-get update && \
	apt-get upgrade -y && \
	apt install pkgconf nfs-common -y && \
	systemctl enable iscsid.service && \
	
	#Create ssh key in defualt directory and display
	< /dev/zero ssh-keygen -t ed25519 -q -N '' && \
	cat ~/.ssh/id_ed25519.pub	
	
	
#Microk8s:
	snap install microk8s --classic && \
	microk8s start && \
	microk8s enable dns registry istio storage helm3 ingress && \
## make helm work
	# mkdir /etc/microk8s && \
	# microk8s.config > /etc/microk8s/microk8s.conf && \
	# export KUBECONFIG=/etc/microk8s/microk8s.conf && \
	# curl https://raw.githubusercontent.com/helm/helm/HEAD/scripts/get-helm-3 | bash && \
##alias to kubectl 
	sudo snap alias microk8s.kubectl kubectl && \
##install kubens
	sudo git clone https://github.com/ahmetb/kubectx /opt/kubectx && \
	sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx && \
	sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens && \
	git clone https://github.com/ahmetb/kubectx.git ~/.kubectx && \
	COMPDIR=$(pkg-config --variable=completionsdir bash-completion) && \
	ln -sf ~/.kubectx/completion/kubens.bash $COMPDIR/kubens && \
	ln -sf ~/.kubectx/completion/kubectx.bash $COMPDIR/kubectx && \
	cat << EOF >> ~/.bashrc 
	export PATH=~/.kubectx:\$PATH 	
EOF

#Add fileshare
	mkdir /mnt/kudalanfs && \
	mount -t nfs 10.234.14.194:/kudalaiot /mnt/kudalanfs && \
	echo "10.234.14.194:/kudalaiot /mnt/kudalanfs nfs defaults,_netdev    0       0" | sudo tee -a /etc/fstab && \

#get repo
	cd /mnt && \
	git clone git@gitlab.com:kieron2/clicks-microk8s-master.git && \

#Storage
#change to directory
	cd /mnt/clicks-microk8s-master/storageclass && \
#create nfs storageclass
	kubectl create -f nfs-storageclass.yaml && \

#LDAP
#change to directory
	cd /mnt/clicks-microk8s-master/ldap && \
#create resources
	kubectl create -f ldap-namespace.yaml && \
#deploy ldap
	kubectl create -f ldap-svc.yaml,ldap-ingress.yaml,ldap-deployment.yaml && \
#deploy phpldap
	kubectl create -f phpldap-svc.yaml,phpadmin-svc.yaml,phpldap.yaml,phpldapadmin.yaml && \

##Timescale:
#change to directory
	cd /mnt/clicks-microk8s-master/timescale && \
#create data folders
	mkdir /mnt/kudalanfs/clicks && \
	mkdir /mnt/kudalanfs/clicks/timescale && \
	mkdir /mnt/kudalanfs/clicks/timescale/data && \
#create resources
	kubectl create -f database-namespace.yaml && \
	kubectl create -f timescale-data-pv.yaml,timescale-data-pvc.yaml,timescale-entry-pv.yaml,timescale-entry-pvc.yaml,timescale-svc.yaml && \
#deploy
	kubectl create -f timescale-manifest.yaml && \

##Grafana
#change to directory
	cd /mnt/clicks-microk8s-master/grafana && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/grafana && \
	mkdir /mnt/kudalanfs/clicks/grafana/data && \
#change data folder permissions
	chmod 777 /mnt/kudalanfs/clicks/grafana/data && \
#create resources
	kubectl apply -f grafana-namespace.yaml && \
	kubectl create -f grafana-data-pv.yaml,grafana-data-pvc.yaml,grafana-share-pv.yaml,grafana-share-pvc.yaml,grafana-svc-copy.yaml && \
#deploy	
	kubectl apply -f grafana-deploy.yaml && \
	
##Prometheus
#change to directory
	cd /mnt/clicks-microk8s-master/prometheus && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/prometheus && \
	mkdir /mnt/kudalanfs/clicks/prometheus/data && \
#change data folder permissions
	chmod 777 /mnt/kudalanfs/clicks/prometheus/data && \
#create resources
	kubectl create namespace monitoring && \
	kubectl create -f prometheus-roles.yaml && \
	kubectl create -f prometheus-config.yaml && \
	kubectl create -f prometheus-svc.yaml && \
	kubectl create -f kube-state-metrics-configs/ && \
	kubectl create -f kubernetes-node-exporter/ && \
	## Remember - pg_stat_statements pq: pg_stat_statements must be loaded via shared_preload_libraries - postgresql.con
	cd /mnt/clicks-microk8s-master/prometheus/postgres-exporter && \
	helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && \
	helm upgrade --cleanup-on-fail --install kudala-postgres-export prometheus-community/prometheus-postgres-exporter --namespace monitoring --version=2.4.0 --values values.yaml && \
	cd /mnt/clicks-microk8s-master/prometheus && \
#deploy
	kubectl create -f prometheus-deploy.yaml && \

##Zookeeper
#change to directory
	cd /mnt/clicks-microk8s-master/zookeeper && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/zookeeper && \
	mkdir /mnt/kudalanfs/clicks/zookeeper/data && \
#create resources
	kubectl create -f zoo-namespace.yaml && \
	kubectl create -f zoo-data-pv.yaml,zoo-data-pvc.yaml,zoo-conf-pv.yaml,zoo-conf-pvc.yaml,zoo-svc.yaml && \
#deploy
	kubectl create -f zoo-deploy.yaml && \

##Kafka
#change to directory
	cd /mnt/clicks-microk8s-master/kafka && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/kafka && \
	mkdir /mnt/kudalanfs/clicks/kafka/data && \
#create resources
	kubectl create -f kafka-namespace.yaml && \
	kubectl create -f kafka-data-pv.yaml,kafka-data-pvc.yaml,kafka-conf-pv.yaml,kafka-conf-pvc.yaml,kafka-svc.yaml && \
#deploy
	kubectl create -f kafka-deploy.yaml && \

##Nifi
#change to directory
	cd /mnt/clicks-microk8s-master/nifi && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/nifi && \
	mkdir /mnt/kudalanfs/clicks/nifi/data && \
#create resources
	kubectl create -f nifi-namespace.yaml && \
	kubectl create -f nifi-data-pv.yaml,nifi-data-pvc.yaml,nifi-svc.yaml && \
#deploy
	kubectl create -f nifi-deployment.yaml && \

##MQTT
#change to directory
	cd /mnt/clicks-microk8s-master/mqtt && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/mqtt && \
	mkdir /mnt/kudalanfs/clicks/mqtt/data && \
	mkdir /mnt/kudalanfs/clicks/mqtt/log && \
#create resources
	kubectl create -f mqtt-namespace.yaml && \
	kubectl create -f mqtt-data-pv.yaml,mqtt-data-pvc.yaml,mqtt-conf-pv.yaml,mqtt-conf-pvc.yaml,mqtt-log-pv.yaml,mqtt-log-pvc.yaml,mqtt-svc.yaml && \
#deploy
	kubectl create -f mqtt-deployment.yaml && \

##NODE-RED
#change to directory
	cd /mnt/clicks-microk8s-master/node-red && \
#create data folders
	mkdir /mnt/kudalanfs/clicks/node-red && \
	mkdir /mnt/kudalanfs/clicks/node-red/data && \
#create resources
	kubectl create -f node-red-namespace.yaml && \
	kubectl create -f node-red-data-pv.yaml,node-red-data-pvc.yaml,node-red-conf-pv.yaml,node-red-conf-pvc.yaml,node-red-svc.yaml && \
#deploy
	kubectl create -f node-red-deployment.yaml